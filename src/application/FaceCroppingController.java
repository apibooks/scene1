package application;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import application.mapper.MACSFaceImformation;
import application.mapper.MACSFaceRectangle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class FaceCroppingController implements Initializable {
	@FXML
	private Button btnStart;
	@FXML
	private TextField txtSurceFolder;
	@FXML
	private TextField txtTargetFolder;
	@FXML
	private TextField txtFaceAPIURL;
	@FXML
	private TextField txtAPIKey;

	public void onCropBtn() throws IOException {
		System.out.println("txtSurceFolder:" + txtSurceFolder.getText());
		System.out.println("txtTargetFolder:" + txtTargetFolder.getText());
		System.out.println("txtFaceAPIURL:" + txtFaceAPIURL.getText());
		System.out.println("txtAPIKey:" + txtAPIKey.getText());

		String sourceFolder = txtSurceFolder.getText();
		try (Stream<Path> stream = Files.walk(Paths.get(sourceFolder), 1)) {
			stream.filter(p -> Files.isRegularFile(p)).forEach(p -> {
				System.out.println("find : " + p);
				try {
					crop(p);
				} catch (IOException | URISyntaxException e) {
					System.out.println("cropping failed : " + p);
				}
			});
		}

	}

	private void crop(Path p) throws IOException, URISyntaxException {
		String fileName = p.getFileName().toString();
		if (!(fileName.endsWith(".png") || fileName.endsWith(".jpg") || fileName.endsWith(".jpeg"))) {
			// 画像ファイル以外はスキップする
			System.out.println("skip : " + p);
			return;
		}
		byte[] image = Files.readAllBytes(p);

		HttpClient httpclient = HttpClients.createDefault();
		URIBuilder builder = new URIBuilder(txtFaceAPIURL.getText());
		HttpPost request = new HttpPost(builder.build());
		request.setHeader("Ocp-Apim-Subscription-Key", txtAPIKey.getText());
		request.setHeader("Content-Type", "application/octet-stream");
		HttpEntity reqEntity = new ByteArrayEntity(image);
		request.setEntity(reqEntity);

		HttpResponse response = httpclient.execute(request);

		HttpEntity entity = response.getEntity();
		String jsonString = EntityUtils.toString(entity).trim();
		ObjectMapper mapper = new ObjectMapper();
		MACSFaceImformation[] info = mapper.readValue(jsonString, MACSFaceImformation[].class);
		List<MACSFaceRectangle> rectangles = Arrays.stream(info).map((i) -> i.faceRectangle)
				.collect(Collectors.toList());

		BufferedImage bi = ImageIO.read(new ByteArrayInputStream(image));
		int counter = 1;
		for (MACSFaceRectangle r : rectangles) {
			String newFileName = fileName.substring(0, fileName.lastIndexOf('.'));
			File destFile = new File(txtTargetFolder.getText() + File.separator + newFileName + "_" + counter + ".png");
			counter++;
			int delta = (int) (r.height * 0.3);
			BufferedImage dest = bi.getSubimage(r.left - delta, r.top - (int) (delta * 1.5), r.width + (delta * 2),
					r.height + (delta * 2));
			try {
				ImageIO.write(dest, "png", destFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		String source = "C:\\face-cropping\\source";
		String target = "C:\\face-cropping\\target";
		String url = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect";
		String key = "d19dfe26fbba4ce4abd3312589625d13";
		txtSurceFolder.setText(source);
		txtTargetFolder.setText(target);
		txtFaceAPIURL.setText(url);
		txtAPIKey.setText(key);
	}

}
